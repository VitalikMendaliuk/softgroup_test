<?php
$pdo = require_once('db.php');
try {
    $stmt = $pdo->prepare('INSERT INTO articles (`title`, `short_text`, `full_text`, `creation_date`) VALUES (:title,  :short_text,  :full_text, CURRENT_TIMESTAMP);');
    if($stmt->execute($_POST)) {
        header('Location: index.php');
    } else {
        die('Error: Something went wrong during adding new record!');
    }
}
catch (PDOException $e) {
    die($e->getMessage());
}