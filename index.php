<?php
$pdo = require_once('db.php');
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>SoftGroup Test</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">SoftGroup Test</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="add.html">Add new article</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<div class="container">
    <div class="row">
        <?php
        if (!isset($_GET['id'])) {
            $stmt = $pdo->query('SELECT * FROM articles');
            if($stmt->rowCount() == 0) echo 'Нічого не знайдено!';
            while ($article = $stmt->fetch(PDO::FETCH_LAZY)): ?>

                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <div class="caption">
                            <h3><?= $article->title ?></h3>
                            <p class="text-muted"><?= $article->creation_date ?></p>
                            <p><?= $article->short_text ?></p>
                            <p class="overflow"><a href="?id=<?= $article->id ?>" class="btn btn-primary pull-right" role="button">Read more</a></p>
                        </div>
                    </div>
                </div>

            <?php endwhile;
        } else {
            $stmt = $pdo->prepare('SELECT * FROM articles WHERE id = ?');
            $stmt->execute([$_GET['id']]);
            if($stmt->rowCount() == 0) echo 'Нічого не знайдено! Можливо ви ввели невірний ID статті.';
            while ($article = $stmt->fetch(PDO::FETCH_LAZY)): ?>
                <div class="heading">
                    <h1><?= $article->title ?></h1>
                    <p class="text-muted"><?= $article->creation_date ?></p>
                </div>
                <p><?= $article->full_text ?></p>

            <?php endwhile;
        }
        ?>
    </div>
</div>


<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>