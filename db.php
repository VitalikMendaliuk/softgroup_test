<?php
$config = [
    'host' => 'localhost',
    'dbname' => 'softgroup_test',
    'user' => 'root',
    'password' => '',
];
$dsn = "mysql:host=" . $config['host'] . ";dbname=" . $config['dbname'] . ";";
$opt = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
);

try {
    return new PDO($dsn, $config['user'], $config['password'], $opt);
} catch (PDOException $e) {
    die('Error: ' . $e->getMessage());
}
